def prover(name: str, age: str) -> str:
    fl = True
    text = 'text'
    for i in range(len(name)):
        if name[i].isdigit():
            fl = False
    if not age.isdigit() or not fl:
        if not age.isdigit() and not fl:
            text = 'Error'
        elif not age.isdigit():
            text = 'Error - age is a number!'
        elif not fl:
            text = 'Error - name is a word!'
        return text
    else:
        text = "{}`s age is {}!".format(name.title(), age)
        return text
