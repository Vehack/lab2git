from flask import Flask, request
from src.prover import prover

app = Flask(__name__)


@app.route('/', methods=['GET'])
def t_f() -> dict:
    name = request.args.get('name')
    age = request.args.get('age')
    if name and age:
        return {'result': prover(name=name, age=age)}
    else:
        return {'result': ['', '']}


if __name__ == '__main__':
    app.run(debug=True, port=5000)
