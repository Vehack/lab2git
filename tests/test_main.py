import src.main
import sys

sys.path.append("Devops2")


def test_main() -> None:
    test_table = [
        ["dima", "18", "Dima`s age is 18!"],
        ["dima", "Dima", "Error - age is a number!"],
        ["18", "18", "Error - name is a word!"],
        ["d1ma", "Dima", "Error"]
    ]
    with src.main.app.test_client() as client:
        for i in range(len(test_table)):
            response = client.get(f'/?name={test_table[i][0]}'
                                  f'&age={str(test_table[i][1])}')
            assert response.status_code == 200
            assert response.json['result'] == test_table[i][2]  # type: ignore
